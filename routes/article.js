const express = require('express');
const router = express.Router();
const model = require('../database/models');
const uuid = require('uuid4');
const jwt = require('express-jwt');

router.get('/article', function(req, res){

    model.Article.find({}, function (err, data) {
        if(err) {
            res.json({
                message: "Error during fetching data"
            });
        }

        res.json(data);
    })
});

router.get('/article/:guid', function(req, res){

    model.Article.findOne({ guid: req.params.guid }, function (err, data) {
        if(err) {
            res.json({
                message: "Error during fetching single article"
            });
        }

        res.json(data);
    })
});

router.post('/article', function(req, res) {
    const article = new model.Article({
        guid: uuid(),
        title: req.body.title,
        description: req.body.description,
        image: req.body.image,
        category: req.body.category,
        comments: []
    });
    article.save(function (err) {
        if (err) {
            res.json({
                message: "Error during inserting data"
            })
        }

        res.json({
            status: true
        })
    });
});

router.put('/article/:guid', function(req, res) {
    model.Article.findOneAndUpdate({ guid: req.params.guid }, {
        title: req.body.title,
        description: req.body.description
    }, function (err, data) {
        if (err) {
            res.json({
                message: "Error during update of article"
            })
        } else {
            res.json(data)
        }
    });
})

router.delete('/article/:guid', function(req, res) {
    model.Article.find({ guid: req.params.guid }).remove(function () {
        res.json({
            status: true
        })
    });
})

module.exports = router;