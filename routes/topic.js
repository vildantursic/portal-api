const express = require('express');
const router = express.Router();
const model = require('../database/models');
const uuid = require('uuid4');


// view recipe
router.get('/topic', function(req, res){

    model.Topic.find(function (err, data) {
        if(err) {
            res.json({
                message: "Error during fetching data"
            });
        }

        res.json(data);
    })
});

router.get('/topic/:guid', function(req, res){

    model.Topic.findOne({ guid: req.params.guid }, function (err, data) {
        if(err) {
            res.json({
                message: "Error during fetching data"
            });
        }

        res.json(data);
    })
});

router.put('/topic/:guid', function(req, res) {
    model.Topic.findOneAndUpdate({ guid: req.params.guid }, {
        $push: { "comments" : { text: req.body.text, username: req.body.username, date: new Date() } }
    }, function (err, data) {
        if (err) {
            res.json({
                message: "Error during update of article"
            })
        } else {
            res.json(data)
        }
    });
})

router.post('/topic', function(req, res) {
    const topic = new model.Topic({
        guid: uuid(),
        title: req.body.title,
        description: req.body.description,
        comments: []
    });
    topic.save(function (err) {
        if (err) {
            res.json({
                message: "Error during inserting topic"
            })
        }

        res.json({
            status: true
        })
    });
});

module.exports = router;
