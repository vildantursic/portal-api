const mongo = require('./connection');

var models = {
    Article: null,
    Topic: null,
    User: null
};

models.Article = mongo.model('Article', {
    guid: { type: String, required: true },
    date: { type: Date, default: Date.now },
    title: { type: String, required: true },
    description: { type: String, required: true },
    image: { type: String, required: true },
    category: { type: String, required: true },
    comments: { type: Array, required: false }
});

models.Topic = mongo.model('Topic', {
    guid: { type: String, required: true },
    date: { type: Date, default: Date.now },
    title: { type: String, required: true },
    description: { type: String, required: true },
    comments: { type: Array, required: false }
});

models.User = mongo.model('User', {
    guid: { type: String, required: true },
    email: { type: String, required: true },
    username: { type: String, required: true, index: { unique: true } },
    password: { type: String, required: true }
});

module.exports = models;
